import React,{Component} from 'react';  
import OwlCarousel from 'react-owl-carousel';  
import 'owl.carousel/dist/assets/owl.carousel.css';  
import 'owl.carousel/dist/assets/owl.theme.default.css';

export class Owl2 extends Component {  
    render()  
    {  
      return (  

/*Owl carousel two*/
<OwlCarousel
    className="owl-theme2"
    items={1}
    autoplay
    loop
    margin={30}
    nav
>
     <div class="item"><img src="./images/website_bitelit.png"/></div>
     <div class="item"><img src="./images/mobileapp_bitelit.png"/></div>
     <div class="item"><img src="./images/digital_marketing_bitelit.jpg"/></div>
     <div class="item"><img src="./images/software_consulting_bitelit.jpg"/></div>
</OwlCarousel>
      )
    }
}
export default Owl2