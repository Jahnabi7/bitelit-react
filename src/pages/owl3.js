import React,{Component} from 'react';  
import OwlCarousel from 'react-owl-carousel';  
import 'owl.carousel/dist/assets/owl.carousel.css';  
import 'owl.carousel/dist/assets/owl.theme.default.css';
import {Card, CardDeck} from 'react-bootstrap';

export class Owl3 extends Component {  
    render()  
    {  
      return (  
/*Owl carousel three*/
<OwlCarousel
    className="owl-theme3"
    items={1}
    autoplay
    loop
    margin={30}
    nav
>
    {/*first template */}
    <CardDeck>
      <Card id="card">
          <div id="skew">
              <h3>Programming in 10+ languages</h3>
          </div>
          <div id="skew1">
              <p className="skew">We are helping SME’s by fluentely programming in Node JS, React, Angular, Mongo DB,
                    Flutter and
                    Java script to name a few.</p>
          </div>
     </Card>
     <Card id="card">
          <div id="skew">
              <h3>10+ Industries Covered</h3>
          </div>
          <div id="skew1">
              <p className="skew">We are focussing on various industries including Education, Media &amp;
                    Entertainment, Retail
                    &amp; e-Commerce Staffing and Automotive</p>
          </div>
     </Card>
    </CardDeck> 
    {/*second template */}
    <CardDeck>
      <Card id="card">
          <div id="skew">
              <h3>10+ Industries Covered</h3>
          </div>
          <div id="skew1">
              <p className="skew">We are focussing on various industries including Education, Media &amp;
                Entertainment, Retail
                &amp; e-Commerce Staffing and Automotive</p>
          </div>
     </Card>
     <Card id="card">
          <div id="skew">
              <h3>Futuristic Solutions</h3>
          </div>
          <div id="skew1">
              <p className="skew">We are building solutions which include Video &amp; Audio streaming, Location
                Enabled Algorithm
                and e-Commerce Solutions</p>
          </div>
     </Card>
    </CardDeck>
    {/*third template */}
    <CardDeck>
      <Card id="card">
          <div id="skew">
              <h3>Futuristic Solutions</h3>
          </div>
          <div id="skew1">
              <p className="skew">We are building solutions which include Video &amp; Audio streaming, Location
                    Enabled Algorithm
                    and e-Commerce Solutions</p>
          </div>
     </Card>
     <Card id="card">
          <div id="skew">
              <h3>Better ROI & Performance</h3>
          </div>
          <div id="skew1">
              <p className="skew">As we focus on SME's our solutions are cost effective and high performance resulitng
                    in better
                    ROI's for our clients.</p>
          </div>
     </Card>
    </CardDeck>
    {/*fourth template */}
    <CardDeck>
      <Card id="card">
          <div id="skew">
              <h3>Better ROI & Performance</h3>
          </div>
          <div id="skew1">
              <p className="skew">As we focus on SME's our solutions are cost effective and high performance resulitng
                    in better
                    ROI's for our clients.</p>
          </div>
     </Card>
     <Card id="card">
          <div id="skew">
              <h3>Programming in 10+ languages</h3>
          </div>
          <div id="skew1">
              <p className="skew">We are helping SME’s by fluentely programming in Node JS, React, Angular, Mongo DB,
                    Flutter and
                    Java script to name a few.</p>
          </div>
     </Card>
    </CardDeck>
</OwlCarousel>
      )
    }
}
export default Owl3