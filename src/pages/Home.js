import React from 'react';
import {
  Navbar, Nav,
  Container,
  Row,
  Col,
  Form,
  Button,
  InputGroup,
  Jumbotron,
  Card
} from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import 'owl.carousel/dist/assets/owl.carousel.min.css';
import Owl1 from '../pages/owl1';  
import Owl2 from '../pages/owl2';
import Owl3 from '../pages/owl3';
 

function Home() {
  return (
    <>
    {/*Navbar*/}
    <Navbar className="bg navbar-fixed-top" fixed="top" expand="lg">
      <Container>
        <Navbar.Brand href="#home" className="brand">
          <img src="./images/logo_small.png" className="logo" />
          <span>
            <b>bitElit</b>
            Technologies
          </span>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link style={{ color: "white" }} href="#home">
              Home
            </Nav.Link>
            <Nav.Link style={{ color: "white" }} href="#works">
              Works
            </Nav.Link>
            <Nav.Link style={{ color: "white" }} href="#services">
              Services
            </Nav.Link>
            <Nav.Link style={{ color: "white" }} href="#about">
              About Us
            </Nav.Link>
            <Nav.Link style={{ color: "white" }} href="#contact">
              Contact Us
            </Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
      {/*Home*/}
      <Container fluid className="home" id="home">
        <Container className="text-white">
          <Row>
            <Col>
              <h1 className="display-4">
                GET IT<br></br>TO GROW
              </h1>
              <p className="lead">
                For us IT is not Information Technology but Innovation
                Technology.
                <br></br>
                We bring aspects of innovation, creativity, advance technology
                and future<br></br> growth in our solutions.<br></br>
                For details contact us<br></br>
              </p>
              <Row>
                <Col xs={12} lg={6}>
                  <div>
                    <InputGroup className="mb-3">
                      <Form.Control
                        type="email"
                        placeholder="you@example.com"
                      />
                      <InputGroup.Append>
                        <Button variant="secondary" type="submit">
                          Ask
                        </Button>
                      </InputGroup.Append>
                    </InputGroup>
                  </div>
                </Col>
              </Row>
            </Col>
            <Col>
              <img src="./images/logo.png" className="logo1" alt="" />
            </Col>
          </Row>
        </Container>
      </Container>
      {/*white div of home*/}
      <Container className="text-center">
        <h1>
          Transform your business with<br></br>
          futuristic solutions
        </h1>
        <p>
          BitElit Technologies is an IT product development company Based in
          Bangalore, India. BitElit provides cutting edge & innovative
          technology solutions to enterprises, SME's and start-ups across Globe.
          We have a team of experienced and talented engineers, strategists and
          marketers to develop user centric and scalable technology products.
        </p>
        <Row className="text-center">
          <Col xs={12} md={4}>
            <img src="./images/innovation.png" className="src" alt="" />
            <Col>
              <h3>Innovation Technology</h3>
              <p>
                We provide not just IT but Innovative Technological Solutions,
                which help our clients grow their business to the next level
              </p>
            </Col>
          </Col>
          <Col xs={12} md={4}>
            <img src="./images/creative.png" className="src" alt="" />
            <Col>
              <h3>Creative Marketing</h3>
              <p>
                With the advent of changing marketing dynamics, keep up to the
                pace with our creative digital marketing services
              </p>
            </Col>
          </Col>
          <Col xs={12} md={4}>
            <img src="./images/support.png" className="src" alt="" />
            <Col>
              <h3>Holistic Support</h3>
              <p>
                As we have the expertise in both IT and Digital Marketing, We
                offer a cross functional support system to our clients
              </p>
            </Col>
          </Col>
        </Row>
      </Container>
      {/*works*/}
      <Container className="text-center" id="works">
        <h1>Our Works</h1>
        <p>
          BitElit Technologies is building innovative technology solutions for
          various industries. These solutions are increasing business
          efficiency, enhancing user experience and resulting in better ROI’s.
          Learning Management System, Location based solution to search business
          and people around, an e-commerce platform for bike rental business, a
          video splitter mobile application to update social status and a
          certified application to access highly secure government servers are
          few among many solutions by BitElit.
        </p>
      </Container>
      {/*Owl carousel one*/}
      <Owl1/>
      {/*Services*/}
      <Container className="mt-5" id="services">
        <Row>
          <Col xs={12} lg={6}>
            <h1>User Friendly Web, Mobile & Digital Marketing Solutions</h1>
            <p>
              We focus on technology as well as Digital Marketing solutions.This
              gives a great leverage to our clients while designing and
              implementing the IT Solutions. The cross reference works wonder
              and makes the final product ready to challenge the world.
            </p>
            <ul className="list">
              <li>
                <img src="./images/icon.png" alt="" />Web applications
              </li>
              <li>
                <img src="./images/icon.png" alt="" />Mobile applications
              </li>
              <li>
                <img src="./images/icon.png" alt="" />Digital marketing
              </li>
              <li>
                <img src="./images/icon.png" alt="" />Software consulting
              </li>
            </ul>
          </Col>
          {/*Owl carousel two*/}
          <Col lg={6}><Owl2/></Col>
        </Row>
      </Container>
      {/*About Us*/}
      <Jumbotron fluid id="about">
        <Container className="one">
          <Row>
            <Col xs={12} xl={4} id="one">
              <p>OUR TEAM OUR STRENGTH</p>
              <h2 className="bold">
                Strength lies in differences, not in similarities
              </h2>
              <p>- STEPHEN COVEY</p>
              <p>
                We are strong team of experienced professionals from diverse
                backgrounds such as Technology (front end, middle ware, back
                end, Android & IOS), Strategy, Operations, Sales and Marketing .
              </p>
            </Col>
            <Col xs={12} xl={4} id="two">
              {/*Profile 1*/}
              <Card className="mb-3">
                <Row className="mt-3">
                  <Col xs={3}>
                    <img
                      src="./images/sushin_bitelit.jpg"
                      className="round"
                      alt=""
                    />
                  </Col>
                  <Col xs={9} className="pt-3">
                    Sushin Pv
                  </Col>
                </Row>
                <Row className="mt-3">
                  <Col>
                    <p>
                      FULL stack developer with expertise in MongoDB, Express
                      JS, Angular and Node JS, PHP, ReactJS, Laravel & Android.
                    </p>
                  </Col>
                </Row>
              </Card>
              {/*Profile 2*/}
              <Card className="mb-3">
                <Row className="mt-3">
                  <Col xs={3}>
                    <img
                      src="./images/rizwan_bitelit.jpg"
                      className="round"
                      alt=""
                    />
                  </Col>
                  <Col xs={9} className="pt-3">
                    Rizwan Sadath
                  </Col>
                </Row>
                <Row className="mt-3">
                  <Col>
                    <p>
                      Developer with focus on Android and IOS technologies such
                      as React Native, Flutter. An expert in UI/UX, 2D, 3D &
                      graphic design & animation.
                    </p>
                  </Col>
                </Row>
              </Card>
              {/*Profile 3*/}
              <Card className="mb-3">
                <Row className="mt-3">
                  <Col xs={3}>
                    <img
                      src="./images/dharshan_bitelit.png"
                      className="round"
                      alt=""
                    />
                  </Col>
                  <Col xs={9} className="pt-3">
                    Dharshan
                  </Col>
                </Row>
                <Row className="mt-3">
                  <Col>
                    <p>
                      4+ years of experience in crafting software using Ruby,
                      RubyOnRails. Focus on developing LBS, Location Based
                      Service, PostgreSQL, PostGIS and PGRouting.
                    </p>
                  </Col>
                </Row>
              </Card>
            </Col>
            <Col xs={12} xl={4}>
              {/*Profile 4*/}
              <Card className="mb-3">
                <Row className="mt-3">
                  <Col xs={3}>
                    <img
                      src="./images/vikas_bitelit.jpg"
                      className="round"
                      alt=""
                    />
                  </Col>
                  <Col xs={9} className="pt-3">
                    Vikas Shrivastava
                  </Col>
                </Row>
                <Row className="mt-3">
                  <Col>
                    <p>
                      15+ years of experience in consulting, operations, project
                      management & startups. MBA from SPJIMR Mumbai, B.E.
                      Marine- cracked IIT JEE 1998.
                    </p>
                  </Col>
                </Row>
              </Card>
              {/*Profile 5*/}
              <Card className="mb-3">
                <Row className="mt-3">
                  <Col xs={3}>
                    <img
                      src="./images/ramees_bitelit.jpg"
                      className="round"
                      alt=""
                    />
                  </Col>
                  <Col xs={9} className="pt-3">
                    Ramees P
                  </Col>
                </Row>
                <Row className="mt-3">
                  <Col>
                    <p>
                      Developer with focus on building user centric UI/UX.
                      Proficient in languages like CSS3, HTML5, Java Script,
                      Angular and React Native.
                    </p>
                  </Col>
                </Row>
              </Card>
              {/*Profile 6*/}
              <Card className="mb-3">
                <Row className="mt-3">
                  <Col xs={3}>
                    <img
                      src="./images/mujeeb_bitelit.jpg"
                      className="round"
                      alt=""
                    />
                  </Col>
                  <Col xs={9} className="pt-3">
                    Mujeeb Rahaman
                  </Col>
                </Row>
                <Row className="mt-3">
                  <Col>
                    <p>
                      Front end developer proficient in languages like CSS3,
                      HTML5 and Java Script. An avid learner learning Angular
                      and React Native.
                    </p>
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
        </Container>
      </Jumbotron>
      {/*white div*/}
      <Container>
        <Container className="two">
          <Row>
            <Col xs={12} lg={6}>
              <p className="lead">DO IT OUR WAY</p>
              <h1><b>Our Unique Selling Proposition</b></h1>
              <p>Diverse team, Cross functional expertise (IT and Digital Marketing), Advance Technology and futiristic focus
          areas are few of our uniqe selling propositions.</p>
            </Col>
            {/*Owl carousel three*/}
            <Col xs={12} lg={6}>
              <Owl3/>
            </Col>
          </Row>
          {/*white div second part*/}
          <Container className="text-center three">
            <p className="lead">We have executed projects for</p>
            <h1>Leading global brands</h1>
            <p>Here are some of the clients we have worked with</p>
            </Container>
            <Container className="four">
              <Row className="pt-5">
                <Col xs={6} md={2}>
                  <img src="./images/client2_bitelit.jpg" alt="" />
                </Col>
                <Col xs={6} md={2}>
                  <img src="./images/client4_bitelit.jpg" alt="" />
                </Col>
                <Col xs={6} md={2}>
                  <img src="./images/client11_bitelit.jpg" alt="" />
                </Col>
                <Col xs={6} md={2}>
                  <img src="./images/client3_bitelit.jpg" alt="" />
                </Col>
                <Col xs={6} md={2}>
                  <img src="./images/client5_bitelit.jpg" alt="" />
                </Col>
                <Col xs={6} md={2}>
                  <img src="./images/client6_bitelit.jpg" alt="" />
                </Col>
              </Row>
          </Container>
        </Container>
      </Container>
      {/*Contact Us*/}
      <Container className="text-center" id="contact">
        <Row>
          <Col xs={12}>
            <h1>Reach out to us</h1>
            <p>You have an idea, but do not have technology capability! you are running a business, which needs digital
          transformation! or you are not able<br></br> to reach to your target customer! Reach out to us and we will build a
          solution for you!<br></br>
          Please drop us an email and BitElit representative will reach you within 24 hours!<br></br></p>
          </Col>
        </Row>
        <Row>
          <Col xs={12}>
            <Container>
          <InputGroup className="mb-3">
                      <Form.Control
                        type="email"
                        placeholder="you@example.com" className="one"
                      />
                      <InputGroup.Append>
                        <Button id="button-addon2" type="submit">
                          Ask
                        </Button>
                      </InputGroup.Append>
                    </InputGroup>
                    </Container>
          </Col>
        </Row>
      </Container>
      {/*Footer*/}
      <Container className="text-center icon">
        <Row>
          <Col>
          <img src="./images/TWITTER-128.png" alt="twitter" className="icon pr-4"/>
          <img src="./images/LINKEDIN-128.png" alt="linkedin" className="icon pr-4"/>
          <img src="./images/FB-128.png" alt="facebook" className="icon pr-4"/>
          <img src="./images/INSTAGRAM-128.png" alt="instagram" className="icon pr-4"/>
          </Col>
        </Row>
        <p><a className="a" href="">Terms</a><a className="a" href="">Privacy</a></p>
        <p>Copyright 2018 All rights reserved - Designed & Developed by bitElit</p>
      </Container>
    </>
  );
}


export default Home;
