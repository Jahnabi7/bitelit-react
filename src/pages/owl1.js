import React,{Component} from 'react';  
import OwlCarousel from 'react-owl-carousel';  
import 'owl.carousel/dist/assets/owl.carousel.css';  
import 'owl.carousel/dist/assets/owl.theme.default.css';

export class Owl1 extends Component {  
    render()  
    {  
      return (  
         /*Owl carousel one*/

    <OwlCarousel
    className="owl-theme"
    items={5}
    autoplay
    loop
    margin={30}
    nav
>
     <div class="item"><img src="./images/works_thinkroutes.jpg"/></div>
     <div class="item"><img src="./images/works_ThinkRoutes_1.png"/></div>
     <div class="item"><img src="./images/works_twobicle.jpg"/></div>
     <div class="item"><img src="./images/works_wcp.jpg"/></div>
    <div class="item"><img src="./images/works_wcp1.jpg"/></div>
</OwlCarousel> 
      ) 
    }  
  }  


export default Owl1