import React from 'react';
import './App.css';
import Home from './pages/Home';
import Privacy from './pages/privacy';
import Terms from './pages/terms';
import {Route} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';


function App() {
  return (
  <div className = "App">

    <Route exact path='/' component={Home}/>
    <Route path='/privacy' component={Privacy}/>
    <Route path='/terms' component={Terms}/>
    
  </div>
  );
}

export default App;



